<?php declare(strict_types=1);

namespace GDXbsv\PServiceBusBundle\Bus;

use GDXbsv\PServiceBus\Message\Message;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

final class BusErrorHandler
{
    public function __construct(public LoggerInterface $logger)
    {
    }

    public function __invoke(\Throwable $t, Message $message): void
    {
        if (class_exists('\Doctrine\DBAL\Exception', false) && $t instanceof \Doctrine\DBAL\Exception) {
            throw $t;
        }

        $level      = LogLevel::ERROR;
        $from       = $message->options->headers['message_type'] ?? 'TYPE NOT DEFINED LOOKS LIKE AN ERROR';
        $logMessage = "Consume from '{$from}' failed.";

        $type = $message->payload::class;

        $this->logger->log(
            $level,
            $logMessage,
            [
                'from'      => $from,
                'type'      => $type,
                'exception' => $t,
                'headers'   => $message->options->headers,
            ]
        );
    }
}
