<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBusBundle;

use GDXbsv\PServiceBusBundle\DependencyInjection\Compiler\AttributesPass;
use GDXbsv\PServiceBusBundle\DependencyInjection\Compiler\BusErrorPass;
use GDXbsv\PServiceBusBundle\DependencyInjection\Compiler\DoctrinePass;
use GDXbsv\PServiceBusBundle\DependencyInjection\Compiler\MiddlewaresPass;
use GDXbsv\PServiceBusBundle\DependencyInjection\Compiler\TransportPass;
use Symfony\Component\DependencyInjection\Compiler\PassConfig;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class PServiceBusBundle extends Bundle
{
    public function build(ContainerBuilder $container): void
    {
        parent::build($container);

        $container->addCompilerPass(new DoctrinePass(), PassConfig::TYPE_BEFORE_OPTIMIZATION, 1000);
        $container->addCompilerPass(new AttributesPass());
        $container->addCompilerPass(new TransportPass());
        $container->addCompilerPass(new BusErrorPass());
        $container->addCompilerPass(new MiddlewaresPass());
    }
}
