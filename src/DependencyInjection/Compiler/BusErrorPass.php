<?php
declare(strict_types=1);

namespace GDXbsv\PServiceBusBundle\DependencyInjection\Compiler;

use GDXbsv\PServiceBus\Bus\ServiceBus;
use GDXbsv\PServiceBusBundle\Bus\BusErrorHandler;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Loader\Configurator\InlineServiceConfigurator;

final class BusErrorPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        if ($container->getParameter('p-service-bus.bus.error_handler_default_set') !== true) {
            return;
        }
        $errorService = (new InlineServiceConfigurator(new Definition(BusErrorHandler::class)))
            ->autowire();
        $closure      = (new InlineServiceConfigurator(new Definition(BusErrorHandler::class)))
            ->autowire()
            ->factory([\Closure::class, 'fromCallable'])
            ->args([InlineServiceConfigurator::processValue($errorService)]);
        $busDef       = $container->getDefinition(ServiceBus::class);
        $busDef->addMethodCall('setErrorHandler', [InlineServiceConfigurator::processValue($closure)]);
    }

}
