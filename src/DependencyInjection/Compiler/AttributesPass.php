<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBusBundle\DependencyInjection\Compiler;

use GDXbsv\PServiceBus\Bus\Handling\Handle;
use GDXbsv\PServiceBus\Bus\Handling\MessageHandleInstruction;
use GDXbsv\PServiceBus\Bus\ServiceBus;
use GDXbsv\PServiceBus\Message\ExternalIn;
use GDXbsv\PServiceBus\Message\ExternalOut;
use GDXbsv\PServiceBus\Message\MessageOptions;
use GDXbsv\PServiceBus\Message\Replay\Replay;
use GDXbsv\PServiceBus\Message\Replay\Replaying;
use GDXbsv\PServiceBus\Message\Replay\ReplayInstruction;
use GDXbsv\PServiceBus\Message\TimeSpan;
use GDXbsv\PServiceBus\Saga\Saga;
use GDXbsv\PServiceBus\Saga\SagaFind;
use GDXbsv\PServiceBus\Saga\SagaFindInstruction;
use GDXbsv\PServiceBus\Saga\SagaMapper;
use GDXbsv\PServiceBus\Saga\SagaPersistence;
use GDXbsv\PServiceBus\Transport\Sns\SnsSqsTransport;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Exception\LogicException;
use Symfony\Component\DependencyInjection\Loader\Configurator\InlineServiceConfigurator;
use Symfony\Component\DependencyInjection\Reference;

/**
 * @internal
 */
class AttributesPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $transports = $container->getParameter('p-service_bus.temp.transports');
        $container->getParameterBag()->remove('p-service_bus.temp.transports');
        $this->handlingInstructions($container, $transports);
        $this->sagaFindInstructions($container);
        $this->replaying($container);
        $this->externalTransport($container, $transports);
        $sagaMapper = $container->getDefinition(SagaMapper::class);

        /** @var list<class-string<Saga>> $sagas */
        $sagas = [];

        foreach ($container->getDefinitions() as $id => $definition) {
            $className = $definition->getClass();
            if (!$className || !class_exists($className, false)) {
                continue;
            }
            if (is_subclass_of($className, Saga::class)) {
                $sagas[] = $className;
                $container->removeDefinition($id);
            }
        }

        $sagaMapper->setArgument('$sagasClasses', $sagas);
    }

    protected function handlingInstructions(ContainerBuilder $container, array $transports): void
    {
        $serviceBus = $container->getDefinition(ServiceBus::class);
        $handlersInstructions = [];
        $handlersMap = [];

        foreach ($container->getDefinitions() as $id => $definition) {
            $handlerClass = $definition->getClass();
            if (!$handlerClass
                || !class_exists($handlerClass, false)
                || $definition->isAbstract()
                || str_starts_with($handlerClass, 'GDXbsv\PServiceBusTestApp')) {
                continue;
            }
            $reflectionClass = new \ReflectionClass($handlerClass);
            foreach ($reflectionClass->getMethods() as $method) {
                $attributes = $method->getAttributes(Handle::class);
                if (count($attributes) === 0) {
                    continue;
                }
                $definition->setLazy(true);
                $arguments = $method->getParameters();
                assert(isset($arguments[0]));
                /** @var \ReflectionNamedType $type */
                $type = $arguments[0]->getType();
                /** @var class-string $messageType */
                $messageType = $type->getName();

                foreach ($attributes as $attribute) {
                    $handleAttr = $attribute->newInstance();
                    if (!isset($transports[$handleAttr->transportName])) {
                        throw new LogicException(
                            "Transport internal with name '{$handleAttr->transportName}' for '{$id}', '{$method->getName()}' is not registered"
                        );
                    }
                    $time = (new InlineServiceConfigurator(new Definition(TimeSpan::class)))
                        ->factory([TimeSpan::class, 'fromSeconds'])
                        ->args([$handleAttr->timeoutSec]);
                    $instruction = (new InlineServiceConfigurator(new Definition(MessageHandleInstruction::class)))
                        ->args(
                            [
                                $messageType,
                                $handleAttr->transportName,
                                $handleAttr->retries,
                                $handleAttr->timeoutSec ? InlineServiceConfigurator::processValue($time) : null,
                                $id,
                                $method->getName(),
                                $method->isStatic(),
                            ]
                        );

                    $handlersInstructions[$messageType][] = InlineServiceConfigurator::processValue($instruction);

                    $handlersMap[$id] = new Reference($id);
                    if (is_subclass_of($handlerClass, Saga::class)) {
                        $handlersMap[$id] = $handlerClass;
                    }
                }
            }
        }

        $serviceBus->setArgument('$handlingInstructions', $handlersInstructions);
        $serviceBus->setArgument('$handlerToObjectMap', $handlersMap);
    }

    protected function sagaFindInstructions(ContainerBuilder $container): void
    {
        $sagaPersistence = $container->findDefinition(SagaPersistence::class);
        $findInstructions = [];
        $findToObjectMap = [];

        foreach ($container->getDefinitions() as $id => $definition) {
            $className = $definition->getClass();
            if (!$className
                || !class_exists($className, false)
                || $definition->isAbstract()
                || str_starts_with($className, 'GDXbsv\PServiceBusTestApp')) {
                continue;
            }
            $reflectionClass = new \ReflectionClass($className);
            foreach ($reflectionClass->getMethods() as $method) {
                $attributes = $method->getAttributes(SagaFind::class);
                if (count($attributes) === 0) {
                    continue;
                }
                $definition->setLazy(true);
                $arguments = $method->getParameters();
                assert(count($arguments) === 2);
                /** @var \ReflectionNamedType $type */
                $type = $arguments[0]->getType();
                /** @var class-string $messageType */
                $messageType = $type->getName();
                /** @var \ReflectionNamedType $type2 */
                $type2 = $arguments[1]->getType();
                assert($type2->getName() === MessageOptions::class);
                /**
                 * @var class-string<Saga> $sagaType
                 * @psalm-suppress UndefinedMethod
                 * @psalm-suppress PossiblyNullReference
                 */
                $sagaType = $method->getReturnType()->getName();
                $find = (new InlineServiceConfigurator(
                    new Definition(SagaFindInstruction::class)
                ))
                    ->args(
                        [
                            $id,
                            $method->getName(),
                        ]
                    );
                $findInstructions[$sagaType][$messageType] = InlineServiceConfigurator::processValue($find);
                $findToObjectMap[$id] = new Reference($id);
            }
        }

        $sagaPersistence->setArgument('$findInstructions', $findInstructions);
        $sagaPersistence->setArgument('$findToObjectMap', $findToObjectMap);
    }

    protected function replaying(ContainerBuilder $container): void
    {
        $replaying = $container->getDefinition(Replaying::class);

        $replayInstructions = [];
        $replayToObjectMap = [];

        foreach ($container->getDefinitions() as $id => $definition) {
            $className = $definition->getClass();
            if (!$className
                || !class_exists($className, false)
                || $definition->isAbstract()
                || str_starts_with($className, 'GDXbsv\PServiceBusTestApp')) {
                continue;
            }
            $reflectionClass = new \ReflectionClass($className);
            foreach ($reflectionClass->getMethods() as $method) {
                $attributes = $method->getAttributes(Replay::class);
                if (count($attributes) === 0) {
                    continue;
                }

                foreach ($attributes as $attribute) {
                    $replayAttr = $attribute->newInstance();
                    $methodName = $method->getName();
                    assert($methodName !== '');

                    $replay = (new InlineServiceConfigurator(
                        new Definition(ReplayInstruction::class)
                    ))
                        ->args(
                            [
                                $id,
                                $methodName,
                            ]
                        );

                    $replayInstructions[$replayAttr->replayName] = InlineServiceConfigurator::processValue($replay);
                    $replayToObjectMap[$id] = new Reference($id);
                }
            }
        }
        $replaying->setArgument('$replays', $replayInstructions);
        $replaying->setArgument('$replayToObjectMap', $replayToObjectMap);
    }

    protected function externalTransport(ContainerBuilder $container, array $transports): void
    {
        $serviceBus = $container->getDefinition(ServiceBus::class);
        $snsSqsTransport = null;
        if ($container->has(SnsSqsTransport::class)) {
            $snsSqsTransport = $container->findDefinition(SnsSqsTransport::class);
        }

        $messageClassMapOut = [];
        $messageNameMapOut = [];
        $messageClassMapIn = [];
        $messageNameMapIn = [];

        foreach ($container->getDefinitions() as $id => $definition) {
            $className = $definition->getClass();
            if (!$className
                || !class_exists($className, false)
                || $definition->isAbstract()
                || str_starts_with($className, 'GDXbsv\PServiceBusTestApp')) {
                continue;
            }
            $reflectionClass = new \ReflectionClass($className);
            $attributesOut = $reflectionClass->getAttributes(ExternalOut::class);
            $attributesIn = $reflectionClass->getAttributes(ExternalIn::class);
            if (count($attributesOut) === 0 && count($attributesIn) === 0) {
                continue;
            }

            foreach (array_merge($attributesOut, $attributesIn) as $attribute) {
                $externalAttr = $attribute->newInstance();
                if (!isset($transports[$externalAttr->transportName])) {
                    throw new LogicException(
                        "Transport external with name '{$externalAttr->transportName}' for '{$externalAttr->externalName}', '{$className}' is not registered"
                    );
                }
                if ($externalAttr instanceof ExternalOut) {
                    $messageClassMapOut[$className] = [
                        $externalAttr->transportName,
                        $externalAttr->externalName,
                    ];
                    $messageNameMapOut[$externalAttr->externalName] = $className;
                }
                if ($externalAttr instanceof ExternalIn) {
                    $messageClassMapIn[$className] = [
                        $externalAttr->transportName,
                        $externalAttr->externalName,
                    ];
                    $messageNameMapIn[$externalAttr->externalName] = $className;
                }
            }
        }

        if ($snsSqsTransport) {
            $snsSqsTransport->setArgument('$messageNameMapIn', $messageNameMapIn);
        }
        $serviceBus->setArgument('$messageInClassMap', $messageClassMapIn);
        $serviceBus->setArgument('$messageOutClassMap', $messageClassMapOut,);
        $serviceBus->setArgument('$messageNameMap', array_merge($messageNameMapIn, $messageNameMapOut));

        $intersect = array_intersect(array_keys($messageNameMapIn), array_keys($messageNameMapOut));
        if (count($intersect) > 0) {
            $intersect = join(', ', $intersect);
            throw new \Exception("You have intersections between In and Out events: ({$intersect})");
        }
    }
}
