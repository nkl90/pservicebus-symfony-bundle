<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBusBundle\DependencyInjection\Compiler;

use GDXbsv\PServiceBus\Bus\ServiceBus;
use GDXbsv\PServiceBus\Bus\TraceableBus;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

final class MiddlewaresPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        $busDefinition = $container->getDefinition(ServiceBus::class);
        foreach ($container->findTaggedServiceIds('service_bus.middleware.in') as $id => $tags) {
            if ($container->getDefinition($id)->getClass() === TraceableBus::class) {
                continue;
            }
            $busDefinition->addMethodCall('addInMiddleware', [new Reference($id)]);
        }
        foreach ($container->findTaggedServiceIds('service_bus.middleware.out') as $id => $tags) {
            if ($container->getDefinition($id)->getClass() === TraceableBus::class) {
                continue;
            }
            $busDefinition->addMethodCall('addOutMiddleware', [new Reference($id)]);
        }
    }

}
