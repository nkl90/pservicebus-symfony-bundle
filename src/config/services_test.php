<?php

declare(strict_types=1);

use GDXbsv\PServiceBus\Bus;
use GDXbsv\PServiceBus\Bus\ServiceBus;
use GDXbsv\PServiceBus\Bus\TraceableBus;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

use function Symfony\Component\DependencyInjection\Loader\Configurator\service;

return static function (ContainerConfigurator $containerConfigurator): void {
    $p = $containerConfigurator->parameters();
    $p->set('p-service-bus.bus.error_handler_default_set', false);


    $services = $containerConfigurator->services();

    $services->defaults()
        ->public()
        ->autowire()
        ->autoconfigure();

    $services->set(TraceableBus::class)
        ->args(
            [
                service(ServiceBus::class),
                service(ServiceBus::class),
                service(ServiceBus::class),
            ]
        );
    $services->alias(Bus::class, TraceableBus::class);
    $services->alias(Bus\ConsumeBus::class, TraceableBus::class);
    $services->alias(Bus\CoroutineBus::class, TraceableBus::class);
};
